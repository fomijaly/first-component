import { useState } from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import './App.css'
import Contact from './components/Contact.jsx';

function App() {
  
  return (
    <>
    <h1>Utilisateurs connectés</h1>
      <section className='list_user'>
        <Contact name={'Ron Weasley'} imageSrc={'https://i.imgur.com/ZN4FhP0.png'} isOnline={true}/>
        <Contact name={'Hermione Granger'} imageSrc={'https://i.imgur.com/38itXzS.png'} isOnline={false}/>
        <Contact name={'Harry Potter'} imageSrc={'https://i.imgur.com/8fPd3y0.jpeg'} isOnline={true}/>
        <Contact name={'Albus Dumbledore'} imageSrc={'https://i.imgur.com/3Dv8drG.png'} isOnline={false}/>
        <Contact name={'Neville Londubat'} imageSrc={'https://i.imgur.com/k8Qeo4D.jpeg'} isOnline={true}/>
        <Contact name={'Severus Rogue'} imageSrc={'https://i.imgur.com/8wprvew.png'} isOnline={true}/>
        <Contact name={'Cédric Diggory'} imageSrc={'https://i.imgur.com/YKuu8mm.jpeg'} isOnline={false}/>
        <Contact name={'Minerva McGonagall'} imageSrc={'https://i.imgur.com/XlhMkM9.jpeg'} isOnline={false}/>
      </section>
    </>
  )
}
export default App


