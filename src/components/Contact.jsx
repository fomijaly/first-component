import React from "react";
import "./../App.css"


function Contact({name, imageSrc, isOnline}){
    return (
        <div className="box_user">
            <div className="user_status">
                <img 
                    className="image_user"
                    src={imageSrc}
                    alt={name}
                />
                {isOnline ? <span className="inline"></span> : <span className="offline"></span>}
            </div>
            <p>{name}</p>
        </div>
    );
}

export default Contact;